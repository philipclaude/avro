// the version should be specified by a macro at compile time
// e.g. #version 410 or #version 330
#version 410

layout( location = 0 ) out vec4 fragColor;

in vec3 v_Position;
in vec3 v_Normal;
in vec3 v_Parameter;

const int ncolor = 256;
uniform float u_umin;
uniform float u_umax;

uniform samplerBuffer colormap;

#define DEBUG 0

void
get_color( float u , out vec3 color ) {

  float umin = 0.;//u_umin;
  float umax = 7.;//u_umax;

//   int indx = int(ncolor*(u - umin)/(umax - umin));
int indx = int(ncolor*(u - umin)/(umax - umin));

  if (indx < 0) indx = 0;
  if (indx > 255) indx = 255;

  float r0 = texelFetch( colormap , 3*(indx) + 0 ).x;
  float g0 = texelFetch( colormap , 3*(indx) + 1 ).x;
  float b0 = texelFetch( colormap , 3*(indx) + 2 ).x;

  color = vec3(r0,g0,b0);

  //if (indx == 36) color = vec3(1,0,1);
}

void main() {
    
    vec3 color = vec3(.8, .8, .8);//vec3(0.8,0.8,0.2);

    int idx  = gl_PrimitiveID;

  
    float f = idx;

    #if DEBUG
    if (idx == 0) color = vec3(1,0,0);
    if (idx == 1) color = vec3(0,1,0);
    if (idx == 2) color = vec3(0,0,1);
    if (idx == 3) color = vec3(1,1,0);
    if (idx == 4) color = vec3(0,1,1);
    if (idx == 5) color = vec3(1,0,1);
    if (idx == 6) color = vec3(.5,.5,.5);

    #else

    get_color(idx, color);
    
    #endif

    fragColor = vec4(color, 1);
}