// the version should be specified by a macro at compile time
// e.g. #version 410 or #version 330
//#version 410

layout( location = 0 ) out vec4 fragColor;

in vec3 v_Position;
in vec3 v_Normal;
in vec3 v_Parameter;
in float v_Index;

const int ncolor = 256;
uniform float u_umin;
uniform float u_umax;
uniform int u_nb_points;

uniform samplerBuffer colormap;

#define DEBUG 0
float random (vec2 uv) {
    return fract(sin(dot(uv,vec2(12.9898,78.233)))*43758.5453123);
}
  
void
get_color( float u , out vec3 color ) {
  // This line turns on the random shading (for structured points)
 //u = random( vec2(v_Index,v_Index) ) * (u_nb_points+1);

  float umin = 0.;
  float umax = u_nb_points +1;

//   int indx = int(ncolor*(u - umin)/(umax - umin));
int indx = int(ncolor*(u - umin)/(umax - umin));

  if (indx < 0) indx = 0;
  if (indx > 255) indx = 255;

  float r0 = texelFetch( colormap , 3*(indx) + 0 ).x;
  float g0 = texelFetch( colormap , 3*(indx) + 1 ).x;
  float b0 = texelFetch( colormap , 3*(indx) + 2 ).x;

  color = vec3(r0,g0,b0);
}

void main() {
    
    vec3 color = vec3(.2, .2, .2);

    float idx  = v_Index; //gl_PrimitiveID;

    get_color(idx, color);

    fragColor = vec4(color, 1);
}