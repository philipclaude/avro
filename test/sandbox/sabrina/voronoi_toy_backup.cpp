
#include "unit_tester.hpp"

#include "graphics/application.h"

#include "library/ckf.h"

#include "avro_config.h" // we need AVRO_SOURCE_DIR to find shaders


// the following two header files are only part of the avro-extensions repository
// #include "gl.h"
#include "graphics/shader.h"

#include "graphics/colormap.h"

using namespace avro;

UT_TEST_SUITE( voronoi_toy )
// this is all borrowed from geometry toy
// set up the view
int width = 500, height = width;

graphics::vec3 eye = {0,0,1};
graphics::vec3 up = {0,1,0};
graphics::vec3 center = {0.0,0.0,0.0};

float fov    = M_PI/4.0;
float aspect = width/height;
float znear  = 0.001;
float zfar   = 100;

// define the transformation matrices, and compute MVP matrix
graphics::mat4 perspective_matrix = graphics::glm::perspective( fov , aspect , znear , zfar );
graphics::mat4 view_matrix = graphics::glm::lookAt( eye , center , up );
graphics::mat4 model_matrix = graphics::glm::identity();
graphics::mat4 mv = view_matrix * model_matrix;
graphics::mat4 mvp = perspective_matrix * mv;

UT_TEST_CASE( test1 )
{
  //also taken from geometry toy
   // initialize OpenGL
  avro_assert_msg( glfwInit() , "problem initializing OpenGL!" );
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow( width , height , "voronoi" , NULL, NULL);
  if (!window) {
    const char* description;
    int code = glfwGetError(&description);

    if (description)
      printf("GLFW error (%d): %s\n",code,description);

    glfwTerminate();
  }
  glfwMakeContextCurrent(window);

  // load GL functions
  gladLoadGL();
  graphics::dumpGLInfo(); // this will be dumpGLInfo() in avro (it isn't necessary but prints some info about OpenGL, GLSL and the GPU)
  //initialize the shader:


  graphics::ShaderProgram shader("voronoi");
  shader.use();

  graphics::gl_index vertex_array;
  GL_CALL( glGenVertexArrays( 1, &vertex_array ) );
  GL_CALL( glBindVertexArray(vertex_array) );
  
  //generate random seeds desired times 3
  int size = 7*3;
  std::vector<graphics::gl_float> seeds;
  srand (time(NULL));

  for (int i = 0; i < size; i ++){
    seeds.push_back(rand() % 10 * 0.01);
    //printf("loop ran, %d point added", i);
  };

  //compute the nearest neighbors here 
  // GEO::NearestNeighborSearch* nns = GEO::NearestNeighborSearch::create(dim,"BNN");

  //  index_t nb_points = 50; //is this the right type?



  // std::vector<graphics::gl_float> seeds = {
  //   0.0, 0.0 , 0.0,
  //   1.0, 0.0 , 0.0,
  //   0.0, 1.0 , 0.0,
  //   0.0, 0.0 , 0.0,
  //   1.0, 1.0 , 0.0,
  //   0.0, -1.0, 0.0,
  // };
  graphics::gl_index points_buffer;
  GL_CALL( glGenBuffers( 1 , &points_buffer) );
  GL_CALL( glBindBuffer( GL_ARRAY_BUFFER , points_buffer ) );
  GL_CALL( glBufferData( GL_ARRAY_BUFFER , sizeof(graphics::gl_float) * seeds.size() , seeds.data() , GL_STATIC_DRAW ) );
  GL_CALL( glBindBuffer( GL_ARRAY_BUFFER , 0 ) );

  

  shader.setUniform("u_ModelViewProjectionMatrix",mvp);
  
  graphics::gl_index colormap_buffer;
  GL_CALL( glGenBuffers( 1 , &colormap_buffer) );

  //get a colormaps and bind it to the colormap buffer
  avro::Colormap colormap;
  colormap.change_style("giraffe");
  index_t ncolor = 256*3;
  GL_CALL( glBindBuffer( GL_TEXTURE_BUFFER , colormap_buffer) );
  GL_CALL( glBufferData( GL_TEXTURE_BUFFER , sizeof(float) * ncolor , colormap.data() , GL_STATIC_DRAW) );
  
  // generate a texture to hold the seeds
  graphics::gl_index seeds_texture;
  GL_CALL( glGenTextures( 1 , &seeds_texture) );
  GL_CALL( glActiveTexture( GL_TEXTURE0 ) );
  GL_CALL( glBindTexture( GL_TEXTURE_BUFFER , seeds_texture) );
  GL_CALL( glTexBuffer( GL_TEXTURE_BUFFER , GL_RGB32F , points_buffer ) );

  // generate a texture to hold the colormap
  GLuint colormap_texture;
  GL_CALL( glGenTextures( 1 , &colormap_texture) );
  GL_CALL( glActiveTexture( GL_TEXTURE0 + 1 ) );
  GL_CALL( glBindTexture( GL_TEXTURE_BUFFER , colormap_texture) );
  GL_CALL( glTexBuffer( GL_TEXTURE_BUFFER , GL_R32F , colormap_buffer ) );

  //generate a texture to hold the nearest neighbors maybe here



  // ensure we can capture the escape key being pressed
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glfwPollEvents();
  glfwSetCursorPos(window, width/2, height/2);

  glDisable(GL_DEPTH_TEST);

  while (true) {

    shader.use();

    // grey-ish (almost white) background color
    float col = 0.9;
    glClearColor (col,col,col, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mvp = perspective_matrix * mv;
    shader.setUniform("u_ModelViewProjectionMatrix", mvp );


    // enable the coordinates texture
    glActiveTexture(GL_TEXTURE0 );
    GLint seeds_location = glGetUniformLocation(shader.handle() , "seeds");
    glUniform1i(seeds_location, 0);


    // enable the colormap texture
    glActiveTexture(GL_TEXTURE0 + 1);
    GLint colormap_location = glGetUniformLocation(shader.handle() , "colormap");
    glUniform1i(colormap_location, 1);


    // draw
    GL_CALL( glBindBuffer(GL_ARRAY_BUFFER, points_buffer ) ); // it doesn't really matter which buffer we bind here since it isn't used

  
    
    GL_CALL( glDrawArrays(GL_POINTS, 0 , 7 ) );


    // swap buffers and wait for user input
    glfwSwapBuffers(window);
    glfwPollEvents();

    // determine if we should exit the render loop
    if (glfwWindowShouldClose(window)) break;
    if (glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS) break;
  }

}
UT_TEST_CASE_END( test1 )

UT_TEST_SUITE_END( voronoi_toy )
