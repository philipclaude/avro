// the version should be specified by a macro at compile time
// e.g. #version 410 or #version 330
#version 410

 layout (points) in;

// uniform mat4 u_ModelViewProjectionMatrix;
// uniform mat4 u_NormalMatrix;
// uniform mat4 u_ModelViewMatrix;

// out vec3 v_Position;
// out vec3 v_Normal;
// out vec3 v_Parameter;

// layout (triangle_strip, max_vertices = 18) out;

// uniform usamplerBuffer connectivity;
// uniform samplerBuffer coordinates;

//test 
// the version should be specified by a macro at compile time
// e.g. #version 410 or #version 330

// the version should be specified by a macro at compile time
// e.g. #version 410 or #version 330

// layout (points) in;

uniform mat4 u_ModelViewProjectionMatrix;
// uniform mat4 u_NormalMatrix;
// uniform mat4 u_ModelViewMatrix;
// uniform int  u_clip;
// uniform vec3 u_clip_center;
// uniform vec3 u_clip_normal;
// uniform int u_render_full;



// out vec3 v_Position;
// out vec3 v_Normal;
// out vec3 v_Parameter;

layout (triangle_strip, max_vertices = 16) out;

//uniform usamplerBuffer connectivity;
uniform samplerBuffer seeds;

// render full square
void render_square(vec3 pt) { 
    //define all four points of the square
    vec3 bl = pt + vec3(-.02, -.02, 0);
    vec3 br = pt + vec3(.02, -.02, 0);
    vec3 tl = pt + vec3(-.02, .02, 0);
    vec3 tr = pt + vec3(.02, .02, 0);

    //top triangle
    
    gl_Position = u_ModelViewProjectionMatrix*vec4(bl, 1.0);
    EmitVertex();
    
    gl_Position = u_ModelViewProjectionMatrix*vec4(tl, 1.0);
    EmitVertex();
    
    gl_Position = u_ModelViewProjectionMatrix*vec4(tr, 1.0);
    //EmitVertex();
    

    gl_PrimitiveID = gl_PrimitiveIDIn;
    
    EmitVertex();
    EndPrimitive();

    
    gl_Position = u_ModelViewProjectionMatrix*vec4(bl, 1.0);
    EmitVertex();

    gl_Position = u_ModelViewProjectionMatrix*vec4(tr, 1.0);
    EmitVertex();

  
    gl_Position = u_ModelViewProjectionMatrix*vec4(br, 1.0);
    //EmitVertex();

    gl_PrimitiveID = gl_PrimitiveIDIn;
    
    EmitVertex();
    EndPrimitive();

}



void
main() {



  // lookup the seed points, taking in glPrimitiveIDIn
  vec3 p0 = vec3(texelFetch( seeds , gl_PrimitiveIDIn ).xy, 0);


  render_square(p0);


  }


