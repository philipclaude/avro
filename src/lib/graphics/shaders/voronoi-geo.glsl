// the version should be specified by a macro at compile time
// e.g. #version 410 or #version 330

#extension GL_ARB_gpu_shader_fp64 : enable

layout (points) in;
precision highp float;

uniform int u_nb_neighbors; 


uniform mat4 u_ModelViewProjectionMatrix;

const int MAX_VERTS = 30;


flat in int[] instance_ID;

out float v_Index; 

layout (triangle_strip, max_vertices = 40) out; 

uniform samplerBuffer seeds;
uniform usamplerBuffer nn; 

void render_poly(vec3[MAX_VERTS] poly, vec3 center, int space){
  vec3 p0 = poly[0];
  for (int i = 0; i < space -2; i++){

    gl_Position = u_ModelViewProjectionMatrix*vec4(p0, 1.0);
    v_Index = float(instance_ID[0]); 
    EmitVertex();
    
    gl_Position = u_ModelViewProjectionMatrix*vec4(poly[i+1], 1.0);
    v_Index = float(instance_ID[0]);
    EmitVertex();
    
    gl_Position = u_ModelViewProjectionMatrix*vec4(poly[i+2], 1.0);
    //gl_PrimitiveID = instance_ID[0];
    v_Index = float(instance_ID[0]);
    EmitVertex();
    EndPrimitive();
  }
}
vec3 calc_intersect(vec3 p1, vec3 p2, vec3 q1, vec3 q2){ // p1 and p2 are seeds, q1 and q2 are borders

  /*
	*  Originally from geogram (https://github.com/BrunoLevy/geogram)
	*  in src/lib/geogram/voronoi/generic_RVD_vertex.h
	*/

  //initialize variables
  float d = 0;
  float l1 = 0;
  float l2 = 0;
  float n = 0;

  //deal with the x
  n = p1.x - p2.x;
  d -= n* (p2.x + p1.x);
  l1 += q2.x * n;
  l2 += q1.x * n;

  //deal with the y
  n = p1.y - p2.y;
  d -= n* (p2.y + p1.y);
  l1 += q2.y * n;
  l2 += q1.y * n;

  //
  d = .5 * d;
  l1 = abs(l1 + d);
  l2 = abs(l2 + d);
  float l12 = l1 + l2;

  if (l12 > 1e-30) { // was 1e-30, changed to 1e-6
    l1 /= l12;
    l2 /= l12;
  }
  else{
    l1 = .5;
    l2 = .5;
  }

  float x0 = l1 * q1.x + l2 * q2.x;
  float y0 = l1 * q1.y + l2 * q2.y;

  return vec3(x0, y0, 0);

}
void calc_poly(vec3 zi, vec3 zj, inout vec3[MAX_VERTS] curr_poly, inout int cspace){
  //cspace is an argument, which is inout as it goes along with curr_poly and represents the actual length of the polygon information in curr_poly at any given moment (ideally)
  int nspace = 0; // nspace is 'new space,' the variable which acts as a pointer to keep track of the next available spot to add a vertex to new_poly

  //where zi is the current site pt and zj is the current nearest neighbor
  vec3 new_poly[MAX_VERTS];
  
  for (int i = 0; i < cspace; i++) {
    vec3 b0;
    vec3 b1;
    if (i == cspace-1){
      b0 = curr_poly[cspace -1];
      b1 = curr_poly[0];
    } 
    else{
      b0 = curr_poly[i];
      b1 = curr_poly[i+1];
    }
    int side1 = 0; 
    int side2 = 0;
    //calculate side 1: determine if the first border point is inside or outside
    if (distance(b0, zi) < distance(b0, zj)) {
      side1 = 1;
    }
    else {
      side1 = -1;
    }
    //calculate side 2: determine if the second border point is inside or outside
    if (distance(b1, zi) < distance(b1, zj)) {
      side2 = 1;
    }
    else {
      side2 = -1;
    }
    vec3 intersect;
    if (side1 != side2){
       intersect = calc_intersect(zi, zj, b0, b1); //b0 and b1 are the current border points
       if (side1 == 1){
         new_poly[nspace] = b0;
         new_poly[nspace+1] = intersect;
         nspace += 2;
       }
       else{
         new_poly[nspace] = intersect;
         nspace += 1;
       }
    }
    else if (side1 == 1){ // meaning both sides are inside
      new_poly[nspace] = b0; // we add in the first border point since we will add in the next point at the next step of the loop
      nspace += 1;
    }
    // otherwise both sides are -1 and we take neither
  }
  curr_poly = new_poly; 
  cspace = nspace; // setting the current space to be the new space so that it is updated for the new polygon.
}
void
main() {
  // Lookup the seed points, taking in instance_ID which we output from the vertex shader. This should be equivalent to glPrimitiveID_In but is not
  int site_idx = instance_ID[0]; 

  vec3 zi = vec3(texelFetch( seeds , site_idx ).xy, 0);
  // This is the initial polygon, we start by initializing it to the polygon which covers the total area of the diagram
  vec3[MAX_VERTS] curr_poly;
  curr_poly[0] = vec3(0, 0, 0);
  curr_poly[1] = vec3(1, 0, 0);
  curr_poly[2] = vec3(1, 1, 0);
  curr_poly[3] = vec3(0, 1, 0);

  int space = 4; 
  for (int i = 1; i < u_nb_neighbors; i ++){
    int nn0 = int(texelFetch( nn, i + (site_idx * u_nb_neighbors)).x); // i + (site_idx * 7)
    vec3 zj= vec3(texelFetch( seeds, nn0).xy, 0); // nn0 is just the index so we need to look up the corresponding point in seeds

    calc_poly(zi, zj, curr_poly, space);
    
    // Below is the radius of security theorem
    float radius = 0;
    // loop over the vertices of the polygon to find the furthest one, the distance from zi to that point becomes the radius. 
    for (int j = 0; j < space; j++ ) {
      vec3 curr_pt = curr_poly[j];
      if(distance(curr_pt, zi) > radius) {
        radius = distance(curr_pt, zi);
      }
    }
    // If the distance between the current nearest neighbor and the current site is greater than twice the radius, stop clipping
    if (distance(zj, zi) > (2.1 * radius)){
      break;
    }
  }
    render_poly(curr_poly, zi, space);
  }
