
#include "unit_tester.hpp"

#include "graphics/application.h"

#include "library/ckf.h"

#include "avro_config.h" // we need AVRO_SOURCE_DIR to find shaders


// the following two header files are only part of the avro-extensions repository
// #include "gl.h"
#include "graphics/shader.h"

#include "graphics/colormap.h"

#include "common/tools.h"

#include <geogram/nn_search.h>

#include <time.h>

#include <iostream>

#include <fstream>

#include "common/process.h"

#include "common/parallel_for.h"

using namespace avro;

UT_TEST_SUITE( voronoi_toy )
// this is all borrowed from geometry toy
// set up the view
int width = 500, height = width;

graphics::vec3 eye = {0.5,0.5,1.5};
graphics::vec3 up = {0,1,0};
graphics::vec3 center = {0.5,0.5,0.0};

float fov    = M_PI/4.0;
float aspect = width/height;
float znear  = 0.001;
float zfar   = 100;

// define the transformation matrices, and compute MVP matrix
graphics::mat4 perspective_matrix = graphics::glm::perspective( fov , aspect , znear , zfar );
graphics::mat4 view_matrix = graphics::glm::lookAt( eye , center , up );
graphics::mat4 model_matrix = graphics::glm::identity();
graphics::mat4 mv = view_matrix * model_matrix;
graphics::mat4 mvp = perspective_matrix * mv;


struct NearestNeighboursCalculator {
  typedef NearestNeighboursCalculator thisclass;
  NearestNeighboursCalculator( coord_t d , const std::vector<real_t>& coords , index_t max_n ) :
    dim(d),
    nb_sites( coords.size() / dim ),
    max_neighbours(max_n),
    sites(coords),
    neighbours(max_n*nb_sites) {
    search = GEO::NearestNeighborSearch::create(dim,"ANN");
    search->set_points( nb_sites , sites.data() );
  }
  ~NearestNeighboursCalculator() {
    delete search;
  }
  void compute( index_t k ) {
    std::vector<double> distance2( max_neighbours ); // +1 because first neighbour is the point itself
    std::vector<index_t> neighbours0( max_neighbours );
    search->get_nearest_neighbors( max_neighbours, &sites[k*dim] , neighbours0.data() , distance2.data()  );
    for (int i = 0; i < max_neighbours; i++)
      neighbours[k*max_neighbours+i] = neighbours0[i];
  }
  void calculate() {
    ProcessCPU::parallel_for( parallel_for_member_callback( this , &thisclass::compute ) , 0 , nb_sites );
  }
  coord_t dim;
  index_t nb_sites;
  index_t max_neighbours;
  const std::vector<real_t>& sites;
  GEO::NearestNeighborSearch* search = nullptr;
  std::vector<GLuint> neighbours;
};

UT_TEST_CASE( test1 )
{
  //also taken from geometry toy
   // initialize OpenGL
  avro_assert_msg( glfwInit() , "problem initializing OpenGL!" );
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* window = glfwCreateWindow( width , height , "voronoi" , NULL, NULL);
  if (!window) {
    const char* description;
    int code = glfwGetError(&description);

    if (description)
      printf("GLFW error (%d): %s\n",code,description);

    glfwTerminate();
  }
  glfwMakeContextCurrent(window);

  // load GL functions
  gladLoadGL();
  graphics::dumpGLInfo(); // this will be dumpGLInfo() in avro (it isn't necessary but prints some info about OpenGL, GLSL and the GPU)
  //initialize the shader:


  graphics::ShaderProgram shader("voronoi",false,{"#version 410"});
  shader.use();

  // graphics::ShaderProgram points_shader("points",false,{"#version 410"});
  // points_shader.use();
  
  graphics::gl_index vertex_array;
  GL_CALL( glGenVertexArrays( 1, &vertex_array ) );
  GL_CALL( glBindVertexArray(vertex_array) );


  // *************************************************************************
  
  //generate random seeds desired times 3
  int num_points = 50;
  int size = num_points*3;


  int MODE = 0; // 0 is random, 1 is square, 2 is hexagonal

  // *************************************************************************
  

  std::vector<graphics::gl_float> seeds;
 srand(24);
if (MODE == 0){

   for (int i = 0; i < size; i ++){
   
    if  ((i  - 2) % 3 == 0){
      
      seeds.push_back(0.0); // make the z coordinate 0
      std::cout << i << std::endl;
    }
    else {
      double val = double(rand()) / double(RAND_MAX);
      //std::cout << i << std::endl;
      seeds.push_back(val);
    }
  //   //std::cout << seeds[i] << std::endl;

  // for (index_t k = 0; k < num_points; k++){
  //   std::cout << random_within(0.0,1.0) << std::endl;
  }
  // };


  
  // real_t point[3];
  // for (index_t k = 0; k < num_points; k++) {
  //   for (coord_t d = 0; d < 3; d++){
  //     if (d < 2) {
  //       point[d] = random_within(0.0,1.0);
  //       //std::cout << point[d] << std::endl;
  //       seeds.push_back(point[d]);
  //     }
      
      
  //     //seeds.push_back(point[d]);
  //     if (d == 2){
  //       seeds.push_back(0.0);
  //     }
  //   }
    
  // }



}
else if (MODE == 1){
  float dx = 1 / (sqrt(num_points) ); //delta y is the same if we're going for a grid
  //std::cout << dx << std::endl;
  for (int i = 0; i < sqrt(num_points) ; i ++ ){
    for (int j = 0; j < sqrt(num_points); j ++ ){
      
      float x = j * dx + dx/2;  
      float y = i * dx + dx/2;
      seeds.push_back(x);
      seeds.push_back(y);
      seeds.push_back(0.0);

    }
   
  }
  num_points = seeds.size()/3;
  std::cout << num_points << std:: endl;

  
}
else if (MODE == 2){


  
  float dx = 1/(sqrt(num_points) ); // idk what number to put here
  float dy = (sqrt(3)/2) * dx;
  //std::cout << sqrt(num_points)/dy << std::endl;
  for (int i = 0; i < 1/dy + 1; i ++ ){
    if ((i % 2) == 0) {
      for (int j = 0; j < 1/(dx) ; j ++ ){
        float x = (j * dx) + dx/2;
        float y = i * dy;
        seeds.push_back(x);
        seeds.push_back(y);
        seeds.push_back(0.0);
      }
  

    }
    else{
      for (int j = 0; j < 1/dx + 1; j ++ ){
        float x = j * dx; 
        float y = i * dy;
        seeds.push_back(x);
        seeds.push_back(y);
        seeds.push_back(0.0);
      }
        
    }

    
  
  }
  num_points = seeds.size()/3;
  std::cout << num_points << std:: endl;
  
}


  std::vector<real_t> seeds_d( seeds.begin() , seeds.end() ); //convert the seeds to double
  
  //compute the nearest neighbors here 
  // start the nearest neighbor timer around here,
  clock_t TIME0 = clock();
   
  // GEO::NearestNeighborSearch* nns = GEO::NearestNeighborSearch::create(3,"BNN"); // number of dimensions is really 2, but we have been using three
  
  //  nns->set_points( num_points , seeds_d.data() );

  index_t nb_neighbors = 30; // should be 50 or 30
  // std::vector<index_t> neighbors(nb_neighbors);
  // std::vector<index_t> total_neighbors(nb_neighbors * num_points); 
  // std::vector<double> neighbors_sq_dist(nb_neighbors);


  // index_t i = 0;
  // for (index_t k=0;k<num_points;k++)
  // {
  //   nns->get_nearest_neighbors( nb_neighbors , k , neighbors.data() , neighbors_sq_dist.data() );

  //   //here we need to resave the neighbors in a new full neighbors array before moving on
    
  //   for (index_t j = 0; j < nb_neighbors; j++){
      
  //     //total_neighbors[((k+1)*i)-1] = neighbors[i];
  //     total_neighbors[i] = neighbors[j];
  //     //std::cout << i << std::endl;
  //     i++;
      
  //   }
    
  // }

 

  NearestNeighboursCalculator nnc( 3 , seeds_d , nb_neighbors );
  nnc.calculate();
  std::vector<GLuint>& neighbors_f = nnc.neighbours;
  clock_t TIME1 = clock();

  int nb_thread = ProcessCPU::maximum_concurrent_threads();
  printf("--> nearest neighbor calculation time = %g seconds\n",real_t(TIME1-TIME0)/real_t(CLOCKS_PER_SEC)/nb_thread);

  // convert nearest neighbor type 
  //std::vector<GLuint> neighbors_f( total_neighbors.begin() , total_neighbors.end() ); 


  graphics::gl_index points_buffer;
  GL_CALL( glGenBuffers( 1 , &points_buffer) );
  GL_CALL( glBindBuffer( GL_ARRAY_BUFFER , points_buffer ) );
  GL_CALL( glBufferData( GL_ARRAY_BUFFER , sizeof(graphics::gl_float) * seeds.size() , seeds.data() , GL_STATIC_DRAW ) );
  GL_CALL( glBindBuffer( GL_ARRAY_BUFFER , 0 ) );

  //bind the nearest neighbor information to the buffer
  graphics::gl_index nn_buffer;
  GL_CALL( glGenBuffers( 1 , &nn_buffer) );
  GL_CALL( glBindBuffer( GL_TEXTURE_BUFFER , nn_buffer ) );
  GL_CALL( glBufferData( GL_TEXTURE_BUFFER , sizeof(GLuint) * neighbors_f.size() , neighbors_f.data() , GL_STATIC_DRAW ) );
  //GL_CALL( glBindBuffer( GL_ARRAY_BUFFER , 0 ) );

  //set uniforms
  shader.setUniform("u_nb_neighbors",int(nb_neighbors));
  shader.setUniform("u_nb_points",int(num_points));
  shader.setUniform("u_ModelViewProjectionMatrix",mvp);

  
  graphics::gl_index colormap_buffer;
  GL_CALL( glGenBuffers( 1 , &colormap_buffer) );

  //get a colormaps and bind it to the colormap buffer
  avro::Colormap colormap;
  colormap.change_style("viridis");
  index_t ncolor = 256*3;
  GL_CALL( glBindBuffer( GL_TEXTURE_BUFFER , colormap_buffer) );
  GL_CALL( glBufferData( GL_TEXTURE_BUFFER , sizeof(float) * ncolor , colormap.data() , GL_STATIC_DRAW) );

 
  // generate a texture to hold the seeds
  graphics::gl_index seeds_texture;
  GL_CALL( glGenTextures( 1 , &seeds_texture) );
  GL_CALL( glActiveTexture( GL_TEXTURE0 ) );
  GL_CALL( glBindTexture( GL_TEXTURE_BUFFER , seeds_texture) );
  GL_CALL( glTexBuffer( GL_TEXTURE_BUFFER , GL_RGB32F , points_buffer ) );

  // generate a texture to hold the colormap
  GLuint colormap_texture;
  GL_CALL( glGenTextures( 1 , &colormap_texture) );
  GL_CALL( glActiveTexture( GL_TEXTURE0 + 1 ) );
  GL_CALL( glBindTexture( GL_TEXTURE_BUFFER , colormap_texture) );
  GL_CALL( glTexBuffer( GL_TEXTURE_BUFFER , GL_R32F , colormap_buffer ) );


  // generate a texture to hold the nearest neighbor information
  graphics::gl_index nn_texture; // does this need to be a different type
  GL_CALL( glGenTextures( 1, &nn_texture) );
  GL_CALL( glActiveTexture( GL_TEXTURE0 + 2) );
  GL_CALL( glBindTexture( GL_TEXTURE_BUFFER, nn_texture) );
  GL_CALL( glTexBuffer( GL_TEXTURE_BUFFER, GL_R32I, nn_buffer) );

  // ensure we can capture the escape key being pressed
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  glfwPollEvents();
  glfwSetCursorPos(window, width/2, height/2);

  glDisable(GL_DEPTH_TEST);
  
  
    shader.use();

    // grey-ish (almost white) background color
    float col = 0.9;
    glClearColor (col,col, col, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mvp = perspective_matrix * mv;
    shader.setUniform("u_ModelViewProjectionMatrix", mvp );

    // //set MAX_VERTS
    // GLint max_verts_loc = glGetUniformLocation(shader.handle(), "MAX_VERTS");
    // glUniform1i(max_verts_loc, 30);
    // enable the coordinates texture
    glActiveTexture(GL_TEXTURE0 );
    GLint seeds_location = glGetUniformLocation(shader.handle() , "seeds");
    glUniform1i(seeds_location, 0);


    // enable the colormap texture
    glActiveTexture(GL_TEXTURE0 + 1);
    GLint colormap_location = glGetUniformLocation(shader.handle() , "colormap");
    glUniform1i(colormap_location, 1);

    //enable the nn search texture
    glActiveTexture(GL_TEXTURE0 + 2);
    GLint nn_location = glGetUniformLocation(shader.handle() , "nn");
    glUniform1i(nn_location, 2);

    // draw
    GL_CALL( glBindBuffer(GL_ARRAY_BUFFER, points_buffer ) ); // it doesn't really matter which buffer we bind here since it isn't used

    /*
    int points_remaining = num_points;
    int first_point_to_draw = 0;
    int call_size = 1e5;
    while (points_remaining > 0){
      //std::cout << "one iteration" << std::endl;
      std::cout << points_remaining << std::endl;
      // std::cout << first_point_to_draw << std::endl;
      if (points_remaining < call_size){
        // std::cout << "if statement" << std::endl;
        // std::cout << first_point_to_draw << std::endl;
        // std::cout << first_point_to_draw + points_remaining << std::endl;
        GL_CALL( glDrawArrays(GL_POINTS, first_point_to_draw , points_remaining ) );
        points_remaining = 0;
      }
      else{
        //std::cout << "test" << std::endl;
        GL_CALL( glDrawArrays(GL_POINTS, first_point_to_draw , call_size ) );
        points_remaining -= call_size;
        first_point_to_draw += call_size;
      }
    } */
  GLuint query;
  //GLuint64 elapsed_time;
 
  glGenQueries(1, &query);
  glBeginQuery(GL_TIME_ELAPSED,query);
 
  // call some OpenGL commands 
  //code borrowed from https://www.lighthouse3d.com/tutorials/opengl-timer-query/
  //glEnable(GL_RASTERIZER_DISCARD);
  GL_CALL( glDrawArrays(GL_POINTS, 0, num_points) );
  //glDisable(GL_RASTERIZER_DISCARD);
  glEndQuery(GL_TIME_ELAPSED);

  GLuint64 time;
  glGetQueryObjectui64v(query, GL_QUERY_RESULT, &time);

  

  //GL_CALL( glDrawArrays (GL_POINTS, 0, num_points) );
  
  //std::cout << time;

  FILE * pFile;
  printf("--> GPU run time = %f seconds\n", time/1e9 );//real_t(TIME3-TIME2)/real_t(CLOCKS_PER_SEC));

  std::ofstream myfile;
  pFile = fopen ("dis_results.txt", "a");

  //fprintf(pFile, "--> nearest neighbor calculation time = %g seconds\n",real_t(TIME1-TIME0)/real_t(CLOCKS_PER_SEC));
  fprintf(pFile, "%12.3e\n", time/1e9 );
  //  fprintf(pFile, "--> GPU run time = %f seconds\n", time/1e9 );
  //fprintf(pFile, "3e6 & %.2E & %.2E \\\\ \n", real_t(TIME1-TIME0)/real_t(CLOCKS_PER_SEC)/nb_thread,  time/1e9 );
  //std::cout << nb_thread;

  //std::cout << "should have written to a file.";
  myfile.close();
 
  glfwSwapBuffers(window);
  while (true) {

    glfwPollEvents();

    // determine if we should exit the render loop
    if (glfwWindowShouldClose(window)) break;
    if (glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS) break;
  }

}
UT_TEST_CASE_END( test1 )

UT_TEST_SUITE_END( voronoi_toy )
