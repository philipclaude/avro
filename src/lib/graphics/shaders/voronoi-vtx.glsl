// the version should be specified by a macro at compile time
// e.g. #version 410 or #version 330
//#version 410
// this code taken from example of tetrahedron/volume toy by Philip
flat out int instance_ID;
layout (location = 0 ) in vec3 a_Position;

// nothing is done here because the geometry shader produces and processes the actual vertices
// but we still need to set gl_Position
void main() {
  gl_Position = vec4(0.0,0.0,0.0,0.0);
  instance_ID = gl_VertexID;
}

